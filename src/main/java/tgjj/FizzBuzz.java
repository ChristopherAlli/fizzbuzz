/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tgjj;

public class FizzBuzz {
    private Integer myInt = 0;
    private String myString = "";

    public FizzBuzz(Integer givenInt) {
        if (givenInt <= 0){
          throw new IllegalArgumentException();
        }
        myInt = givenInt;
    }

    public String toString(){
        return myString;
    }

    public void divisible(){
        if (myInt % 3 == 0 && myInt % 5 ==0){
            myString = myString + "FizzBuzz";
        }
        else if(myInt % 5 == 0){
            myString = myString + "Buzz";
        }
        else if(myInt % 3 == 0){
            myString = myString + "Fizz";
        }
        else{
            myString = myString.valueOf(myInt);
        }
    }
}
